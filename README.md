# 🌱 Plant Disease Recognition Using Deep Learning

## Overview 🌐
This project focuses on the early and accurate detection (classification) of plant diseases using advanced deep learning techniques 🧠. Leveraging the PlantVillage dataset 🌿, we aim to refine detection methods and contribute to smarter agricultural practices and better crop protection. For detailed information about the project, please refer to the `report.pdf` file.

## Data Acquisition 📊
The project utilizes the [PlantVillage dataset](https://github.com/spMohanty/PlantVillage-Dataset), which includes a diverse collection of high-resolution images of healthy and diseased plant leaves 🍃. The dataset is publicly available and annotated with labels indicating specific plant species and disease conditions.

## Data Preprocessing and Augmentation 🔧
Preprocessing involves resizing images to consistent dimensions, critical for YOLOv8 model input. Augmentation techniques employed include:
- 🔄 **Rotation and Flipping:** Random rotations and flips to simulate different orientations.
- 📏 **Scaling and Cropping:** Random scaling and cropping, followed by resizing to original dimensions.
- 🎨 **Color Adjustments:** Variations in brightness, contrast, saturation, and hue.
- 🔳 **Gaussian Blur and Noise:** Addition of Gaussian blur and noise.
- 🌈 **Advanced Techniques:** CLAHE for contrast adjustments, and hue, saturation, and value adjustments for color enhancements.

## Model Selection 🤖
The project employs [YOLOv8 architecture](https://docs.ultralytics.com) for state-of-the-art performance in object detection tasks.

## Installation and Setup ⚙️

To set up this project, you'll need to have Python 3.9.18 installed along with the following libraries. The project also relies on specific versions of PyTorch, torchvision, and torchaudio compatible with CUDA 11.8.

### Prerequisites
- Python 3.9.18
- [CUDA 11.8](https://developer.nvidia.com/cuda-11-8-0-download-archive) (for GPU support)

### Python Libraries
To install the required Python libraries, you can use the following command:

```bash
pip install numpy matplotlib seaborn Pillow albumentations scikit-learn ultralytics
```

This command installs:
- `numpy`: For numerical operations.
- `matplotlib`: For data visualization and plotting.
- `seaborn`: For statistical data visualization.
- `Pillow (PIL)`: For image processing tasks.
- `albumentations`: For advanced image augmentation techniques.
- `scikit-learn`: For machine learning tools, including model evaluation metrics.

We will also need pytorch compatible with CUDA 11.8. Here is a [link](https://pytorch.org/get-started/locally/).

### Additional Setup
Make sure that your environment is set up with CUDA 11.8 to leverage GPU acceleration for PyTorch. Verify the installation with:

```python
import torch
print(torch.__version__)
print(torch.cuda.is_available())
```

This will confirm the PyTorch version and whether CUDA is available in your environment.


## Running the Project 🏃‍♂️

This section guides you through the steps to run the Plant Disease Classification project, from preprocessing to model evaluation.

### Step 1: Download and Prepare the Dataset 📥
- 📦 Download the complete dataset and trained model weights from [here](https://www.kaggle.com/datasets/matjputk/plantvillage-dataset-with-augmentations). This ZIP file contains:
  - 📁 `dataset` folder: Includes `train`, `validation`, `test`, and `hard_test` datasets.
  - 💾 `runs` folder: Contains the trained model weights.
- 🌱 Optionally, the original images from the PlantVillage dataset can be downloaded here: [PlantVillage Dataset](https://github.com/spMohanty/PlantVillage-Dataset/tree/master/raw/color).

### Step 2: Extract and Organize Files 🗂️
- 🔓 Extract the ZIP file to a known directory. Ensure the `dataset` and `runs` folders are correctly placed.

### Step 3: Launch the Jupyter Notebook 🚀
- 💻 Open your terminal or command prompt and navigate to the directory containing the `plant_disease_classification.ipynb` Jupyter Notebook.
- 📚 Start Jupyter Lab or Jupyter Notebook:
  ```bash
  jupyter lab
  # or
  jupyter notebook
  ```

### Step 4: Explore and Run the Notebook 🧪
- 📖 In the Jupyter interface, open `plant_disease_classification.ipynb`.
- 🌟 The notebook provides a comprehensive walkthrough of the project:
  - 📂 **Data Loading**: Loading the original images from the PlantVillage dataset.
  - 🔧 **Preprocessing**: Resizing and applying other preprocessing techniques to the images.
  - 🎨 **Augmentation**: Augmenting the dataset for robustness.
  - 🔀 **Dataset Splitting**: Splitting the data into training, validation, and testing sets. Additionally, creating a challenging 'hard test' subset.
  - 🏋️‍♂️ **Model Training**: Training the YOLO model on the processed dataset. Users can either train the model themselves or use the pre-trained weights from the `runs` folder.
  - 📊 **Evaluation**: Evaluating the model's performance on the standard test set and the 'hard test' set.

### Step 5: Model Evaluation 📈
- ✔️ Follow the instructions in the notebook to evaluate the model. This includes loading the trained model, running it on the test datasets, and analyzing the performance metrics.

### Additional Information 📝
- 🔍 Make sure the paths to the dataset and model weights in the notebook match your directory structure.
- ⏳ If you choose to train the model, note that it might be time-consuming depending on your hardware capabilities.
- 🛠️ Check that all required dependencies are installed and, if using GPU acceleration, that CUDA is correctly set up.
